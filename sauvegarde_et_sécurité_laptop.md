# Sauvegarde du laptop

Pour sauvegarder mon laptop, j'utilise un utilitaire qui s'appelle Sauvegardes qui est déjà installé et qui me permet de sauvegarder directement les fichiers ou je veux et à l'heure que je souhaite.
Il me permet aussi de restaurer ma sauvegarde
Les fichiers enregistrés sont chiffrés à l'aide d'une clé que moi seul connais afin de ne pas risquer un vol de données en cas de perte du disque dur.

Actuellement, je développe un script rsync avec cron afin de sauvegarder mon /home toutes les heures.
Je prévois de faire tourner mon script en daemon pour avoir mon /home synchronisé en temps réel en plus.
Toutes ses sauvegardes seront faites sur mon disque dur
[Voici mon script que j'actualise](./Script/save.sh)
Je prévois également deux clés pour permettre à mon grub ou windows de pouvoir Boot si jamais j'ai un problème au démarrage
Pour les sauvegardes sur windows, j'utilise un utilitaire déjà fournit par windows afin de sauvegarder sur mon disque dur les données importantes. 
Les données sont déposée en brut sur le disque dur et ne sont pas chiffrés  mais c'est prévu aussi.

# Securité laptop

Pour mes mots de passe, j'utilise le gestionnaire de mots de passe keepass2 sur mon ubuntu et windows car il est assez pratique pour récupérer les mots de passes rentré et est sécurisé.
Je n'utilise pas d'antivirus sur mon ubuntu car ce n'est pas necessaire.
Par contre, sur ma session Windows, j'utilise l'antivirus de base, celui de Windows.
Je prévois dans un futur plus ou moins proche d'acheter un Yubikey