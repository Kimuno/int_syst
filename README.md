# int_syst

## Repository pour les rendus

[Objectif du projet fil-rouge](./Objectif_du_projet.md)

[Sécurisation et Sauvegarde du laptop](./sauvegarde_et_sécurité_laptop.md)

[Mise en place et installation du projet](./mise_en_place_projet.md)


[Script de sauvegarde BDD](./Script/.backupconf)

----- en cours ------

[Sauvegarde AWX et Restauration](./Sauvegarde_restauration_awx.md)

----- en cours ------

[Fonctionnement awx](./Fonctionnement_awx.md)

----- en cours ------
