# Centralisation des logs

## Point de départ

Pour mon projet, j'ai décidé de mettre en place un système de log ou tout les logs seront centralisé sur ma VM de sauvegarde

Pour cela, j'ai décidé d'utiliser rsyslog

## Rsyslog

C'est un outil utilisé dans les système type UNIX qui permet de centraliser les logs de plusiseurs Machines

## Situation

Dans mon cas, j'ai décidé de centraliser les logs de ma machine awx ainsi que la machine Serveur web sur la machine de sauvegarde

## Mise en place

Il faut tout d'abord installer Rsyslog (si'il n'est pas déjà installé sur la distribution) sur les machines concernées

    yum install rsyslog
    
Par la suite, il faut lancer le service, faire en sorte qui se lance au boot et vérifier si il a bien été lancé

    systemctl start rsyslog
    systemctl enable rsyslog
    systemctl status rsyslog
    
Ensuite, il faut se rendre sur la machine qui va centrer les logs (dans mon cas, la VM de sauvegarde) et rentrer dans le fichier de log

    sudo vim /etc/rsyslog.conf
    
Pour mettre en place ce système de log, il faut décider de quel protocole on va se servir.

Dans mon cas, j'ai décidé d'utiliser l'udp et le tcp et le port sur lequel on va l'utiliser

En rentrant dans le fichier de configuration, il faut se rendre à la fin du fichier pour rentrer notre conf
Les lignes qui suivent indique le protcole utilisé, le port et le fait que ce soit cette machine qui soit le serveur

    $ModLoad imudp
    $UDPServerRun 514

    $ModLoad imtcp
    $InputTCPServerRun 514

Les lignes suivantes indiquent ou seront stocké les logs récupéré ( dans ce cas, le hostname sera le nom et je prend tout les niveau de logs)

$template RemoteLogs,"/var/log/%HOSTNAME%/%PROGRAMNAME%.log"
*.* ?RemoteLogs
& ~

Ensuite, on indique on seront stocké les fichiers auxilières

$WorkDirectory /var/lib/rsyslog

Il faut ensuite configurer les autres machines :

Il suffit de rentrer le type de facility et de criticité (*.* pour indiquer qu'on prend tout)

*.* @@10.0.0.4:514

Il faut ensuite ouvrir le port 514 sur les machines

    sudo firewall-cmd --permanent --add-port=514/udp
    sudo firewall-cmd --permanent --add-port=514/tcp

et désactiver SELINUX








