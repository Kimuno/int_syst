# Explication du fontionnement d'AWX

## Introduction


Ansible Awx est un outil d'intégration et de déploiement d'application et/ou de composants divers 

Awx est un outil open source qui a été crée par par RedHat compatible avec Aws et la conteneurisation Docker
Awx est à différencier de Ansible Tower qui est sa version non open source
Si je m'arretais là, Awx ne serait pas très différent de Ansible

Or, Awx fait des choses en plus : 

C'est un outil d’orchestration de playbooks Ansible, fournissant de la ségrégation de rôles, de la centralisation de logs et du REST API


## Monitoring

Tout d'abord, quand l'interface graphique Awx est lancé, on se rend compte d'une chose:

C'est également un outil de monitoring

![](./Images/Screen_monitoring.png)

Il permet de consulter les informations les plus importantes comme l'état des jobs et voir si ils sont en echec ou réussi,le nombre de worker disponible, les hôtes en echec d'exécution de playbook, le nombre d'inventaires etc....

## Jobs

Dans Awx, il existe ce que l'on appelle des jobs.

Les jobs correspondent à une instance d'awx qui va lancer un playbook ansible sur une liste d'hôtes

Ansible comprends un onglet Job ou seront listé tout les jobs.

![](./Images/Jobs.png)


