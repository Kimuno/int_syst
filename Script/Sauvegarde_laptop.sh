#!/bin/bash
sudo rsync -av - --delete --log-file=/var/log/$(date +%Y%m%d)_rsync.log $HOME /media/maxim/Maxtor/Sauvegarde
retour=$?
log=/var/log
DATE=$(date +"Script enregistré le %d/%m/%Y à %H  %M  %S")
echo $DATE
echo le code retour est $retour



case $retour in
0)
echo Sauvegarde réussie
echo" le code retour est $retour";;
1)
echo Erreur de syntaxe ou d’usage
echo" le code retour est $retour";;
2)
echo Protocole incompatible;;
3)
echo Erreurs pendant la sélection des fichiers;;
4) 
echo l'action demandée n'est pas supportée;;
5)
echo Erreur au démarrage du protocole client-serveur;;
10)
echo Erreur d’entrée sortie réseau;;
11)
echo Erreur d’entrée sortie sur un fichier;;
12)
echo Erreur dans le flot de données du protocole de rsync;;
13)
echo Erreurs dans le programme de diagnostiques;;
14)
echo Erreur dans le code IPC;;
15)
echo Processus enfant crashé;;
16)
echo Processus enfant terminé anormalement;;
19)
echo Après avoir reçu le signal SIGUSR1;;
20)
echo Après avoir reçu un des signaux SIGINT, SIGTERM, SIGHUP;;
21)
echo Erreur retournée par waitpid();;
22)
echo Erreur d’allocation mémoire;;
23)
echo Transfert partiel;;
24)
echo Des fichiers on disparus côté serveur;;
25)
echo Suppressions n’ont effectuées à cause de –max-delete;;
30)
echo Temps d’attente dépassé pendant le transfert;;
35)
echo Temps d’attente dépassé en attendant une connection;;
*)
echo Excuse me what the fuck;;
esac

