# Scripts de sauvegardes

## MySQLdump

Pour ce script, j'ai crée un fichier .backuconf qui contient toutes mes variables d'environnement où l'utilisateur va rentrer ces informations afin de connecter (Login et password), le nombre de version de sauvegarde qu'il veut conserver, une gestion des erreurs via un fichier de log et un message qui indiquera l'erreur.
Ce script peut sauvegarder plusieurs BDD, toutes les BDD ou une seul en particulier.

Pour ce script, il faut que le fichier .backuconf ai seulement des droits de lecture pour être executé. En faisant cela, le script b'aura pas besoin d'un administrateur pour s'executer

Mon script va chercher les variables d'environnement en le parsant (j'aurais pu utiliser source mais je voulais m'entrainer sur les commandes linux en faisant des filtres) 

## Script de sauvegarde du laptop

Sauvegarde avec Rsync de mon $HOME avec Rsync
Utilisation  Crontab sur mon pc


## Script Sauvegarde awx

Je sauvegarde sur ma VM hôte awx la base de données en json contenant toutes les données rentrée sur awx

Je transfert en scp vers ma VM dédiée aux sauvegardes le fichier grâce au ssh
