# Mise en place du projet

Pour mon projet, j'ai décidé d'utiliser AWX, la version Open-Source d’Ansible Tower (outil d’orchestration de playbooks Ansible, fournissant de la ségrégation de rôles, de la centralisation de logs et du REST API)

Pour installer AWX, il faut une VM avec au moins 4 go de RAM, 2 CPU, 20 go de place sur le disque dur, la version 2.4+ d'Ansible et Docker d'installé

## Installation

J'ai donc choisi une machine Centos7 pour mon projet à laquelle j'ai attribué 4 go de ram, un disque dur virtuel de 20 go et 2 processeur sur virtualbox

J'ai par la suite ajouté une carte host-only et un nat afin de pouvoir avoir accès à internet


Une fois l'installation de Centos terminée, j'ai mis une ip fixe sur ma machine afin de pouvoir lancer l'interface web Awx avec cette ip.

Pour l'installation, il faut ajouter le dépôt epel : 

`yum -y install epel-release `

Il faut ensuite désactiver le pare-feu et SELinux

    systemctl stop firewalld
    sed -i s/SELINUX=enforcing/SELINUX=disabled/ /etc/selinux/config
    setenforce 0

Par la suite, il faut installer les dépendances nécessaires au fonctionnement de AWX

    yum -y install git gettext ansible docker nodejs npm gcc-c++ bzip2
    yum -y install python-docker-py


Démarrer et activer le service Docker

    systemctl start docker
    systemctl enable docker


Déploiement de AWX par le dépot GIT

    git clone https://github.com/ansible/awx.git

    cd awx/installer/
    ansible-playbook -i inventory install.yml

Vérification du déploiement

    docker logs -f awx_task

Il suffit ensuite de se connecter à l'interface web avec l'ip attribuée (10.0.0.2 dans mon cas)

L'identifiant de base : admin
Mot de passe de base : password

![](./Images/Capture_web_interface.png)
